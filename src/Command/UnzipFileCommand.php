<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Services\Unzip\ZipArchive;

class UnzipFileCommand extends Command
{
    protected static $defaultName = 'app:unzip-archive';

    private $unzip_client;

    function __construct(ZipArchive $unzip_client)
    {
        $this->unzip_client = $unzip_client;

        parent::__construct();
    }
    
    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if($this->unzip_client->unzip("public/upload/file.zip") === TRUE){
            $output->writeln("file unzip successfully");
            return Command::SUCCESS;
        }else{
            $output->writeln("the file did not unzip");
            return Command::FAILURE;
        }
    }
}