<?php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Services\Uploaders\GuzzleHttpClient;

class UploadFileCommand extends Command
{
    protected static $defaultName = 'app:upload-file';

    private $client;

    function __construct(GuzzleHttpClient $client)
    {
        $this->client = $client;

        parent::__construct();
    }
    
    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        if (($status = $this->client->upload("http://www.astramodel.cz/b2b/export_xml_PSs6t5prnOYaHfTOUI-6XNF6m.zip")) == 200) {
            $output->writeln("file loaded successfully");
            return Command::SUCCESS;
        } else {
            if($status == 1001) {
                $output->writeln("create manual folder public/upload");
            } else {
                $output->writeln("the file did not load");
            }
            return Command::FAILURE;
        }
    }
}