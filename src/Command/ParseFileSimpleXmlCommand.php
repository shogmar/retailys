<?php
namespace App\Command;

use App\Services\Parsers\SimpleXmlAlgorithms;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use App\Services\Parsers\SimpleXmlParser;
use Symfony\Component\Stopwatch\Stopwatch;

class ParseFileSimpleXmlCommand extends Command
{
    protected static $defaultName = 'app:parse-simple-xml';

    function __construct(SimpleXmlParser $parser)
    {
        $this->parser = $parser;

        parent::__construct();
    }

    protected function configure(): void
    {
        // ...
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $time = new Stopwatch();
        if (file_exists('public/upload/export_full.xml') === false) {
            if (file_exists('public/upload/file.zip') === true) {
                $output->writeln("no file. app:unzip-archive");
                return Command::FAILURE;
            } else {
                $output->writeln("no file. app:upload-file");
                return Command::FAILURE;
            }
        }
        $algorithms = new SimpleXmlAlgorithms();
        $parser = new SimpleXmlParser();
        $parser->load("public/upload/export_full.xml", $algorithms);
        $time->start('parseSimpleXMLService');
        list($products, $countProducts) = $parser->parse();
        $event = $time->stop('parseSimpleXMLService');
        if (empty($products) === true || empty($countProducts) === true) {
            $output->writeln("the file is empty or contains no information");
            return Command::FAILURE;
        }
        foreach ($products as $code => $item_pr) {
            $output->writeln("ProductCode: ".$code." name: ".$item_pr["name"]);
            if (array_key_exists("partsItem", $item_pr)) {
                foreach ($item_pr["partsItem"] as $code_part_item => $name_part_item) {
                    $output->writeln("Code_part_item: ".$code_part_item." name_part_item: ".$name_part_item." ProductCode: ".$code);
                }
            }
        }
        $output->writeln("Total products: ".$countProducts);
        $output->writeln($event);
        return Command::SUCCESS;
    }
}