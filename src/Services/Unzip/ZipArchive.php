<?php
namespace App\Services\Unzip;

class ZipArchive
{
  /**
   * Unzip File
   *
   * @param string $openPath
   * @param string $extractTo
   * @return bool
   */
    public function unzip($openPath = 'public/upload/file.zip', $extractTo = 'public/upload/')
    {
        $zip = new \ZipArchive;
        $res = $zip->open($openPath);
        if ($res === TRUE) {
            $zip->extractTo($extractTo);
            $zip->close();
            return TRUE;
        } else {
            return FALSE;
        }
    }
}