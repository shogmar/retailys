<?php
namespace App\Services\Parsers;

class SimpleXmlAlgorithms implements AlgorithmsParserXmlInterface {

    private $products = [];

    private $countProducts = 0;

    public function chainFunction(\SimpleXMLIterator $node)
    {
        $this->searchProduct($node);
        return [$this->products, $this->countProducts];
    }

    private function searchProduct(\SimpleXMLIterator $node)
    {
        for ($node->rewind(); $node->valid(); $node->next()) {
            if ($node->key() == "items") {
                foreach ($node->items->item as $prod) {
                    if (count($prod->parts) !== 0) {
                        foreach ($prod->parts->part->item as $part) {
                            $this->products[(string)$prod->attributes()["code"]]["name"] = (string)$prod->attributes()["name"];
                            $this->products[(string)$prod->attributes()["code"]]["partsItem"][(string)$part->attributes()["code"]] = (string)$part->attributes()["name"];
                        }
                    } else {
                        $this->products[(string)$prod->attributes()["code"]]["name"] = (string)$prod->attributes()["name"];
                    }
                    $this->countProducts++;
                }
            }
        }
    }
}