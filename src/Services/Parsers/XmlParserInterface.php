<?php
namespace App\Services\Parsers;

interface XmlParserInterface
{
    /**
     * Load resurce
     */
    public function load(string $path, AlgorithmsParserXmlInterface $simple_algorithms);
    /**
     * parse xml
     */
    public function parse();
}