<?php
namespace App\Services\Parsers;

class SimpleXmlParser implements XmlParserInterface
{
    private $obj_xml = NULL;

    private $simple_algorithms;

    public function load(string $path, AlgorithmsParserXmlInterface $simple_algorithms)
    {
        $this->parse_url = parse_url($path);
        $this->simple_algorithms = $simple_algorithms;
        $this->obj_xml = simplexml_load_file($path, "SimpleXMLIterator");
        if (is_null($this->obj_xml)) return false; else return true;
    }

    public function parse()
    {
        if (is_null($this->obj_xml)) return false;
        return $this->simple_algorithms->chainFunction($this->obj_xml);
    }

    public function getObj()
    {
        return $this->obj_xml;
    }
}