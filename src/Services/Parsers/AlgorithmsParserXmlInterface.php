<?php
namespace App\Services\Parsers;

interface AlgorithmsParserXmlInterface {
    function chainFunction (\SimpleXMLIterator $node);
}