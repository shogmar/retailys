<?php
namespace App\Services\Uploaders;

use GuzzleHttp\Client;

class GuzzleHttpClient
{
    public $fullpath;
    public $parse_url;

    /**
     * Upload file
     *
     * @param string $fullpath
     * @param string $localPath
     * @return int
     */
    public function upload(string $fullpath = "http://www.astramodel.cz/b2b/export_xml_PSs6t5prnOYaHfTOUI-6XNF6m.zip", $localPath = 'public/upload/file.zip')
    {
        $this->fullpath = $fullpath;
        $this->parse_url = parse_url($fullpath);
        if (!file_exists('public/upload')) {
            if (mkdir("/public/upload/", 0700, true) === false) {
                return 1001;
            }
        }
        $client = new Client([
          'base_uri' => $this->parse_url["scheme"]."://".$this->parse_url["host"],
          'timeout'  => 2.0,
        ]);
        $status_code = $client
          ->request('GET', $this->parse_url['path'], ['sink' => $localPath, 'timeout' => 600])
          ->getStatusCode();
        
        return $status_code;
    }
}